const server = require('../app');
const request = require('supertest');

jest.mock('../lib/UserAuth');

const UserAuth = require('../lib/UserAuth');

// close the server after each test
afterEach(() => {
  server.close();
});

describe('Routes', () => {
  test('POST /api/auth/login should respond as expected', async () => {
    const expected = { _id: '1', username: 'justin', password: 'User' };

    expected.validatePassword = jest.fn().mockReturnValueOnce(true);

    const expectedRes = { username: 'justin' };
    UserAuth.findOne.mockReturnValueOnce(expected);
    const response = await request(server).post('/api/auth/login').send({ username: 'justin', password: 'password' });
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(expectedRes);
  });
});
