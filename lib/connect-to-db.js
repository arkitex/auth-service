const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const timeout = (ms) => new Promise(resolve => setTimeout(resolve, ms));

const connectToDb = async (uri, options) => {
  try {
    await mongoose.connect(uri, options);
    console.log('connected to database');
  } catch (err) {
    if (err.message && err.message.match(/failed to connect to server .* on first connect/)) {
      console.log(new Date(), String(err));
      // Wait for a bit, then try to connect again
      await timeout(10000);
      console.log('Retrying first connect...');
      await connectToDb(uri, options);
    } else {
      // Some other error occurred.  Log it.
      console.error(new Date(), String(err));
    }
  }
};

module.exports = (dbOptions = {}) => {
  const {
    host = 'localhost',
    port = '27017',
    database = 'myapp'
  } = dbOptions;

  const options = {
    poolSize: 5,
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true
  };

  let uri = `mongodb://${host}:${port}/${database}`;
  return connectToDb(uri, options);
};
